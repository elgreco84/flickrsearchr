//
//  ViewController.swift
//  FlickrBrowser
//
//  Created by Rick Roberts on 3/7/16.
//  Copyright © 2016 Steep Mountain Tech. All rights reserved.
//

import UIKit

class ViewController: UIViewController {

    override func viewDidLoad() {
        super.viewDidLoad()
        // Do any additional setup after loading the view, typically from a nib.
    }
    
    override func viewDidAppear(animated: Bool) {
        super.viewDidAppear(animated)
        searchPhotos("airplane")
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    func searchPhotos(tags: String) {
        
        let baseURL = "https://api.flickr.com/services/rest/?&method=flickr.photos.search&format=json&nojsoncallback=1"
        let apiString = "&api_key=777c4d0045e4366f08e14c831485a63b"
        let searchString = "&tags=\(tags)"
        
        guard let requestURL = NSURL(string: baseURL + apiString + searchString) else { return }
        let session = NSURLSession.sharedSession()
        
        let task = session.dataTaskWithURL(requestURL, completionHandler: { data, response, error -> Void in
            guard let d = data else { return }
            do {
                let result = try NSJSONSerialization.JSONObjectWithData(d, options: NSJSONReadingOptions.AllowFragments)
                print(result)
            } catch let e {
                print(e)
            }
        })
        
        task.resume()
    }


}



