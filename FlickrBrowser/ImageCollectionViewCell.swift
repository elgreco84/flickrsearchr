//
//  ImageCollectionViewCell.swift
//  FlickrBrowser
//
//  Created by Rick Roberts on 3/8/16.
//  Copyright © 2016 Steep Mountain Tech. All rights reserved.
//

import UIKit

struct ImageCollectionViewCellViewModel {
    let imageURL: NSURL
}

class ImageCollectionViewCell: UICollectionViewCell {
    
    var viewModel: ImageCollectionViewCellViewModel? {
        didSet {
            guard let vm = viewModel else { print("Error: No ImageCollectionViewCellViewModel found"); return }
            print(vm)
        }
    }
    
}
